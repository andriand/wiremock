# README #

Dockerfile for wiremock


## Usage ##

### Create mocks: ###
```
		fun stubForBodyTransformer(): MappingBuilder = WireMock
				.get(urlMatching("/body-transformer/.*"))
				.willReturn(
						WireMock.aResponse()
								.withHeader("Content-Type", "application/json;charset=UTF-8")
								.withHeader("Transfer-Encoding", "chunked")
								.withStatus(200)
									// https://github.com/opentable/wiremock-body-transformer
								.withBody("""{"var":"$(var)","got":"it"}""")
								.withTransformers("body-transformer")
								.withTransformerParameter("urlRegex", "/template/(?<var>.*?)")
				)

		fun stubForResponseTemplate(): MappingBuilder = WireMock
				.get(urlMatching("/response-template/.*"))
				.willReturn(
						WireMock.aResponse()
								.withHeader("Content-Type", "application/json;charset=UTF-8")
								.withHeader("Transfer-Encoding", "chunked")
								.withStatus(200)
									// http://wiremock.org/docs/response-templating/
								.withBody("{{request.url}}")
								.withTransformers("response-template")
				)
```

### Send requests:###


```bash
$ curl http://localhost:17777/body-transformer/456
{"var":"null","got":"it"}

$ curl http://localhost:17777/response-template/3456
/response-template/3456
```