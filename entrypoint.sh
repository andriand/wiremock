#!/bin/sh
set -x

if [ -n "$CMD_PROXY_TARGET" ]; then 
	export PROXY_OPTS="--proxy-all=${CMD_PROXY_TARGET}"; 
else 
	export PROXY_OPTS=""; 
fi

env | sort

java -cp "/wiremock/wiremock.jar:/wiremock/wiremock-body-transformer.jar" \
	com.github.tomakehurst.wiremock.standalone.WireMockServerRunner \
	--global-response-templating \
	--verbose \
	--extensions com.opentable.extension.BodyTransformer \
	--port ${CMD_PORT} \
	--https-port ${CMD_HTTPS_PORT} \
	--container-threads ${CMD_CONTAINER_THREADS} \
	--print-all-network-traffic ${CMD_PRINT_ALL_NETWORK_TRAFFIC} \
	${PROXY_OPTS}
